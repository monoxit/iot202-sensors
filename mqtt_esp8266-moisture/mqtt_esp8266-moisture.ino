/*mqtt_esp8266-moisture.ino
   This scketch is based on mqtt_esp8266.ino included with the pubsubclient by Nick O'Leary
   pubsubclientライブラリに添付のmqtt_esp8266.ino by Nick O'Learyに基づく
   
   センサー出力-220K+TOUT+100K-GND
   土壌湿気センサー値の目安
   0  ~300     乾燥
   300~700     湿り気あり
   700~950     水の中

   Copyright (c) 2019 Masami Yamakawa
   setup_wifi(), reconnect() and parts of callback() functions are
     Copyright (c) 2008-2015 Nicholas O'Leary
   This software is released under the MIT License.  
*/

// ESP8266 WiFi などのライブラリを使うと指定
// ライブラリ：ある機能を容易に使うことができるようにあらかじめ作られたプログラム集
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// ネットワーク環境に合わせて修正
const char* ssid = "YOUR_SSID";
const char* password = "YOUR_WIFI_PASSWORD";
const char* mqtt_server = "mqtt.example.com";

// 無線LANに接続するためのライブラリを「espClient」という名前で使えるようにする
// サーバーとの接続にSSL/TLSを使うときは「WiFiClientSecure」を使う
// WiFiClientSecure espClient;
WiFiClient espClient;

// MQTTクライアントライブラリを「client」という名前で使えるようにする
PubSubClient client(espClient);

long lastMillis = 0;
char msg[50];

void setup() {
  WiFi.mode(WIFI_STA);
  Serial.begin(115200);
  // WiFiにつなぐ
  setup_wifi();
  // MQTTクライアント機能に、接続先アドレスとポート番号を設定
  client.setServer(mqtt_server, 1883);
}

// WiFiにつなぐ部分
// プログラムの他の部分にsetup_wifi()があると実際はここの{から}が実行される
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// MQTTサーバーへ接続する部分
void reconnect() {
  // 接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // デバイスID 「IOT999」で接続開始
    if (client.connect("IOT999")) {
      Serial.println("connected");
      // 接続できたらトピック「outTopic」に
      //「IOT999:hello world」を投稿
      client.publish("outTopic", "IOT999:hello world");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// ずっと繰り返す部分
void loop() {

  // MQTTサーバーに接続していなかったらreconnect()の部分を実行
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // 電源ONからの経過ミリ秒数をnowへ保存
  long now = millis();

  //最後のメッセージ送信から2000ミリ秒以上経過していたら
  if (now - lastMillis > 2000) {
    // now（今の経過ミリ秒数）をlastMillis（前のミリ秒数）へ保存
    lastMillis = now;

    // 土壌湿気センサーの値をTOUT(=A0)から読み込みmoistureへ保存
    int moisture = analogRead(A0);
    Serial.print("Publish message: ");
    Serial.println(moisture);
    //トピック「IOT999/moisture」にメッセージを投稿
    snprintf (msg, sizeof(msg) - 1, "%i", moisture);
    client.publish("IOT999/moisture", msg);
  }
}
