/*mqtt_esp8266-adxl.ino
   This scketch is based on mqtt_esp8266.ino included with the pubsubclient by Nick O'Leary
   pubsubclientライブラリに添付のmqtt_esp8266.ino by Nick O'Learyに基づく

   SCL-IO5, SDA-IO4, VDD(VCC)-3.3V, GND-GND

   Copyright (c) 2019 Masami Yamakawa
   setup_wifi(), reconnect() and parts of callback() functions are
     Copyright (c) 2008-2015 Nicholas O'Leary
   This software is released under the MIT License.
*/

// ESP8266 WiFi などのライブラリを使うと指定
// ライブラリ：ある機能を容易に使うことができるようにあらかじめ作られたプログラム集
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h> // I2C通信のプログラムを容易にするライブラリ

// ネットワーク環境に合わせて修正

const char* ssid = "YOUR_SSID";
const char* password = "YOUR_WIFI_PASSWORD";
const char* mqtt_server = "mqtt.example.com";

// 加速度センサーADXL345のアドレス
// センサーチップのアドレス選択ピンのハイまたはローで
// 0x1Dまたは0x53を選択可
const uint8_t deviceAddress = 0x1D;

// 無線LANに接続するためのライブラリを「espClient」という名前で使えるようにする
// サーバーとの接続にSSL/TLSを使うときは「WiFiClientSecure」を使う
// WiFiClientSecure espClient;
WiFiClient espClient;

// MQTTクライアントライブラリを「client」という名前で使えるようにする
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];

void setup() {
  WiFi.mode(WIFI_STA);
  pinMode(13, OUTPUT);     // 13番ピンを出力に設定
  Serial.begin(115200);
  // WiFiにつなぐ
  setup_wifi();
  // MQTTクライアント機能に、接続先アドレスとポート番号を設定
  client.setServer(mqtt_server, 1883);

  // I2C通信機能の準備をする
  Wire.begin();
  // デバイスIDを取得して、シリアルモニタに出力する
  // センサーのレジスタの0x00番地にI2CデバイスIDが保存されている。
  uint8_t res = readByte(0x00); //I2C通信でセンサーの0x00番地レジスタの内容を読み込む
  Serial.print("I_AM:");
  Serial.println(res,HEX);

  // ADXL345の設定
  // DATA_FORMATレジスタ（0x31番地）に0x00（最大2gまで計測）を設定する
  writeByte(0x31, 0x00);
  // POWER_CTLレジスタ（0x2d番地）に0x08（測定）を設定
  writeByte(0x2d, 0x08);
}

// WiFiにつなぐ部分
// プログラムの他の部分にsetup_wifi()があると実際はここの{から}が実行される
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// MQTTサーバーへ接続する部分
void reconnect() {
  // 接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // デバイスID 「IOT999」で接続開始
    if (client.connect("IOT999")) {
      Serial.println("connected");
      // 接続できたらトピック「outTopic」に
      //「IOT999:hello world」を投稿
      client.publish("outTopic", "IOT999:hello world");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// ずっと繰り返す部分
void loop() {

  // MQTTサーバーに接続していなかったらreconnect()の部分を実行
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
  long now = millis();
  if (now - lastMsg > 1000) {
    //最後のメッセージ送信から1000ミリ秒以上経過していたら
    lastMsg = now;
    int16_t x = getX();
    Serial.print("Publish message: ");
    Serial.println(x);
    //トピック「IOT999/x」にメッセージを投稿
    snprintf (msg,sizeof(msg)-1, "%i", x);
    client.publish("IOT999/x", msg);
  }
}

int16_t getX(){
  int16_t x = readByte(0x32);
  x = x | ((int16_t)readByte(0x33) << 8);
  return x;
}

int16_t getY(){
  int16_t y = readByte(0x34);
  y = y | ((int16_t)readByte(0x35) << 8);
  return y;
}

int16_t getZ(){
  int16_t z = readByte(0x36);
  z = z | ((int16_t)readByte(0x37) << 8);
  return z;
}

void writeByte(uint8_t registerAddress, uint8_t data) {
  // ADXL345（アドレス0x1D）へのI2Cによる送信開始
  Wire.beginTransmission(deviceAddress);
  // アクセス先レジスタ番号送信
  Wire.write(registerAddress);
  // データーの送信
  Wire.write(data);
  /// 送信終了
  Wire.endTransmission();
}

uint8_t readByte(uint8_t registerAddress){
  uint8_t res;
  // ADXL345（アドレス0x1D）へのI2Cによる送信開始
  Wire.beginTransmission(deviceAddress);
  // アクセス先レジスタ番号送信
  Wire.write(registerAddress);
  // 送信終了
  Wire.endTransmission();
  // ADXL345（アドレス0x1D）へデーター読み込み要求
  Wire.requestFrom(deviceAddress,(uint8_t)1);
  // データーを読み込み「res」に保存
  res = Wire.read();
  return res;//「res」を結果として返す
}
