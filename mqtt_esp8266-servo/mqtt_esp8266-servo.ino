/*mqtt_esp8266-servo.ino
 * This scketch is based on mqtt_esp8266.ino included with the pubsubclient by Nick O'Leary
 * pubsubclientライブラリに添付のmqtt_esp8266.ino by Nick O'Learyに基づく
 *
 * Copyright (c) 2019 Masami Yamakawa
 * setup_wifi(), reconnect() and parts of callback() functions are
 *   Copyright (c) 2008-2015 Nicholas O'Leary
 * This software is released under the MIT License.
 * http://opensource.org/licenses/mit-license.php
 * 
 * サーボの信号線（黄色）-マイコンのIO16
 * サーボの赤- USB-TTL変換モジュールの5V
 * サーボの黒（茶）- GND（マイナスレール）
*/

// ESP8266 WiFiなどのライブラリを使うと指定
// ライブラリ：ある機能を容易に使うことができるようにあらかじめ作られたプログラム集
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <Servo.h> //サーボモーターのライブラリ

// ネットワーク環境に合わせて修正

const char* ssid = "WIFI_SSID";
const char* password = "WIFI_PSWD";
const char* mqtt_server = "broker.hivemq.com";

const char* mqttTopicPrefix = "mxit/";

// 無線LANに接続するためのライブラリを「espClient」という名前で使えるようにする
// サーバーとの接続にSSL/TLSを使うときは「WiFiClientSecure」を使う
// WiFiClientSecure espClient;
WiFiClient espClient;

// MQTTクライアントライブラリを「client」という名前で使えるようにする
PubSubClient client(espClient);

// Servoライブラリを「myservo」という名前で使えるようにする
Servo myservo;

// JSON形式を展開するのに使う一時保存場所を200バイト分確保
StaticJsonDocument<200> doc;

String mqttDeviceId;
String mqttServoTopic;
String mqttStateTopic;

void setup() {
  WiFi.mode(WIFI_STA);
  pinMode(13, OUTPUT);     // LEDをつなげた13番ピンを出力に設定
  Serial.begin(115200);
  // WiFiにつなぐ
  setup_wifi();
  // MQTTクライアント機能に、接続先アドレスとポート番号を設定
  client.setServer(mqtt_server, 1883);
  // MQTTクライアント機能に、サーバーからメッセージを受信したときに実行する部分を設定
  client.setCallback(callback);

  // MQTTデバイスIDをセットする
  String serialNumber = String(ESP.getChipId(), HEX);
  mqttDeviceId = "MXIT-" + serialNumber;

  // MQTTトピックをセットする
  mqttServoTopic += mqttTopicPrefix;
  //mqttServoTopic += serialNumber + "/";
  mqttServoTopic += "servo";
 
  mqttStateTopic += mqttTopicPrefix;
  mqttStateTopic += serialNumber;
  mqttStateTopic += "/state";
  
  //サーボモーターが接続されているピンを16番ピンに設定
  myservo.attach(16);
  //サーボモーターの角度を０度にする
  myservo.write(0);
}

// WiFiにつなぐ部分
// プログラムの他の部分にsetup_wifi()があると実際はここの{から}が実行される
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// MQTTサーバーからメッセージを受信したときに実行させる部分
void callback(char* topic, byte* payload, unsigned int length) {
  digitalWrite(13,HIGH); // LED ON
  // Serial・・・の行はプログラムの主要部分でない
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // MQTTブローカーから受信したペイロードのJSON形式データを展開してdocにセットする
  //  ペイロードには{"angle":45}のようなJSONテキストが入っている
  DeserializationError error = deserializeJson(doc, payload);

  if(!error){ // 展開エラーでなければ
    // angleの値をangle変数へ代入する
    //   ペイロードが{"angle":45}のときはangleに45が代入される
    int angle = doc["angle"];
    // サーボモーターをangleで指定した角度にする
    if(angle >= 0 && angle <= 180){
      myservo.write(angle);
      Serial.print("Move to ");
      Serial.println(angle);
    }else {
      Serial.println("Angle should between 0-180");
    }
  }else {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
  }
  digitalWrite(13,LOW); // LED OFF
}

// MQTTサーバーへ接続する部分
void reconnect() {
  // 接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // デバイスIDで接続開始
    if (client.connect(mqttDeviceId.c_str())) {
      Serial.println("connected");
      // 接続できたら状態通知用トピックにメッセージパブリッシュ
      String payload = mqttDeviceId;
      payload += " started";
      client.publish(mqttStateTopic.c_str(), payload.c_str());
      Serial.print(mqttStateTopic);
      Serial.print(":");
      Serial.println(payload);
      // ...さらにサーボトピックに申し込み
      client.subscribe(mqttServoTopic.c_str());
      Serial.print("Subscribe:");
      Serial.println(mqttServoTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// ずっと繰り返す部分
void loop() {

  // MQTTサーバーに接続していなかったらreconnect()の部分を実行
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}
