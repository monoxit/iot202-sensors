/* mqtt_esp8266-door.ino
 * This scketch is based on mqtt_esp8266.ino included with the pubsubclient by Nick O'Leary
 * pubsubclientライブラリに添付のmqtt_esp8266.ino by Nick O'Learyに基づく
 *
 * VCC-SW-IO5-R10k-GND
 * 
 * Copyright (c) 2019 Masami Yamakawa
 * setup_wifi(), reconnect() and parts of callback() functions are
 *   Copyright (c) 2008-2015 Nicholas O'Leary
 * This software is released under the MIT License.
 * http://opensource.org/licenses/mit-license.php
*/

// ESP8266 WiFi などのライブラリを使うと指定
// ライブラリ：ある機能を容易に使うことができるようにあらかじめ作られたプログラム集
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// ネットワーク環境に合わせて修正
const char* ssid = "YOUR_WIFI_SSID";
const char* password = "YOUR_WIFI_PASSWD";
const char* mqtt_server = "broker.hivemq.com";

// 公開MQTTブローカの他の利用者とトピックが重複しないようにするための接頭文字
const char* mqttTopicPrefix = "mxit/";

// ドアセンサが接続されているピン番号
const int doorPin = 5;

// 無線LANに接続するためのライブラリを「espClient」という名前で使えるようにする
// サーバーとの接続にSSL/TLSを使うときは「WiFiClientSecure」を使う
// WiFiClientSecure espClient;
WiFiClient espClient;

// MQTTクライアントライブラリを「client」という名前で使えるようにする
PubSubClient client(espClient);

unsigned long lastMillis = 0; // 前回センサ確認時の経過ミリ秒数に０ミリ秒を初期化
int lastDoor; //　前回センサ状態確認時のドア開閉状態を記憶する変数を用意

String mqttDeviceId;
String mqttTopic;

void setup() {
  WiFi.mode(WIFI_STA);
  pinMode(doorPin,INPUT); //5番ピンを入力モードにセット
  Serial.begin(115200);
  // WiFiにつなぐ
  setup_wifi();
  // MQTTクライアント機能に、接続先アドレスとポート番号1883を設定
  // SSL/TLSで接続するときは8883など暗号化接続用のポート番号を指定する
  client.setServer(mqtt_server, 1883);

  // チップIDを読み取りserialNumberに代入
  String serialNumber = String(ESP.getChipId(), HEX);

  // mqttDeviceId変数にMXIT-とチップIDを組み合わせたデバイスIDを代入
  mqttDeviceId = "MXIT-" + serialNumber;

  // あらかじめセット済の接頭文字チップIDからMQTTトピック構築する
  // mqttTopicは最終的に「mxit/abcd1234/door」となる
  mqttTopic += mqttTopicPrefix;
  mqttTopic += serialNumber;
  mqttTopic += "/door";

  // 前回センサ確認時のドア開閉状態に現状と逆の状態をセット
  lastDoor = !digitalRead(doorPin);
}

// WiFiにつなぐ部分
// プログラムの他の部分にsetup_wifi()があると実際はここの{から}が実行される
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// MQTTサーバーへ接続する部分
void reconnect() {
  // 接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // デバイスIDを指定して接続し接続できたら
    if (client.connect(mqttDeviceId.c_str())) {
      Serial.println("connected");
    } else { //接続に失敗したら
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // 5秒待つ
      delay(5000);
    }
  }
}

// ずっと繰り返す部分
void loop() {

  // MQTTサーバーに接続していなかったらreconnect()の部分を実行
  if (!client.connected()) {
    reconnect();
  }
  client.loop(); //MQTTクライアントのキープアライブなど裏方の処理を実行させる

  // 電源ONからの経過ミリ秒数をnowへ保存
  unsigned long now = millis();

  //最後の確認から100ミリ秒以上経過していたら
  if (now - lastMillis > 100) {
    // now（今の経過ミリ秒数）をlastMillis（前のミリ秒数）へ保存
    lastMillis = now;

    // ドアセンサの値（HIGH(1)かLOWか(0)）を読み込みdoorへ保存
    int door = digitalRead(5);

    // もしdoorの状態が前回確認時と異なっていたら
    if(door != lastDoor){
      // lastDoor（前のドアの状態）にdoor（今のドアの状態）を保存
      lastDoor = door;

      // MQTTペイロードを組み立て
      // payloadにはJSON形式のメッセージ「例{"device":abcd1234,"door":1}」が入る
      String payload = "{\"device\":\"";
      payload += mqttDeviceId;
      payload += "\",";
      payload += "\"door\":";
      payload += door;
      payload += "}";

      // MQTTブローカに, トピックとペイロードをパブリッシュ
      client.publish(mqttTopic.c_str(), payload.c_str());
      
      Serial.print(mqttTopic);
      Serial.print(":");
      Serial.println(payload);
    }
  }
}
