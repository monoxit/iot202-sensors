/*mqtt_esp8266-adxllib.ino
   This scketch is based on mqtt_esp8266.ino included with the pubsubclient by Nick O'Leary
   pubsubclientライブラリに添付のmqtt_esp8266.ino by Nick O'Learyに基づく

   SCL-IO5, SDA-IO4, VDD(VCC)-3.3V, GND-GND

   Copyright (c) 2019 Masami Yamakawa
   setup_wifi(), reconnect() and parts of callback() functions are
     Copyright (c) 2008-2015 Nicholas O'Leary
   This software is released under the MIT License.
   http://opensource.org/licenses/mit-license.php
*/

// ESP8266 WiFi などのライブラリを使うと指定
// ライブラリ：ある機能を容易に使うことができるようにあらかじめ作られたプログラム集
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <ADXL345.h> // ADXL345センサーを容易に使えるようにするライブラリ

// ネットワーク環境に合わせて修正
const char* ssid = "YOUR_WIFI_SSID";
const char* password = "YOUR_WIFI_PASSWORD";
const char* mqttServer = "broker.hivemq.com";

const char* mqttTopicPrefix = "mxit/";

// 無線LANに接続するためのライブラリを「espClient」という名前で使えるようにする
// サーバーとの接続にSSL/TLSを使うときは「WiFiClientSecure」を使う
// WiFiClientSecure espClient;
WiFiClient espClient;

// MQTTクライアントライブラリを「client」という名前で使えるようにする
PubSubClient client(espClient);

// ADXL345ライブラリを「accel」という名前で使えるようにする
// ADXL345センサーのアドレスはスタンダードの「1D」（16進数）を指定する
// パラメタをADXL345_ALTとすると代替アドレスの「53」（16進数）を指定できる
ADXL345 accel(ADXL345_STD);

unsigned long lastMsg = 0;
String mqttDeviceId;
String mqttTopic;

void setup() {
  WiFi.mode(WIFI_STA);
  Serial.begin(115200);
  // WiFiにつなぐ
  setup_wifi();
  // MQTTクライアント機能に、接続先アドレスとポート番号を設定
  client.setServer(mqttServer, 1883);

  // MQTTデバイスIDをセットする
  String serialNumber = String(ESP.getChipId(), HEX);
  mqttDeviceId = "MXIT-" + serialNumber;

  // MQTTトピックをセットする
  mqttTopic += mqttTopicPrefix;
  mqttTopic += serialNumber;
  mqttTopic += "/accel";

  // I2C通信機能の準備をする
  Wire.begin();
  // センサのデバイスIDを取得して、シリアルモニタに出力する
  byte deviceID = accel.readDeviceID();
  Serial.print("I_AM:");
  Serial.println(deviceID, HEX);

  // ADXL345のサンプリングレートを100Hzに設定
  accel.writeRate(ADXL345_RATE_100HZ);
  // ADXL345の測定範囲を2Gに設定
  accel.writeRange(ADXL345_RANGE_2G);
  // ADXL345の測定を開始させる
  accel.start();
}

// WiFiにつなぐ部分
// プログラムの他の部分にsetup_wifi()があると実際はここの{から}が実行される
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// MQTTサーバーへ接続する部分
void reconnect() {
  // 接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // デバイスIDを指定して接続開始
    if (client.connect(mqttDeviceId.c_str())) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // 5秒待つ
      delay(5000);
    }
  }
}

// ずっと繰り返す部分
void loop() {

  // MQTTサーバーに接続していなかったらreconnect()の部分を実行
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // 加速度センサから最新のデータを読み取りライブラリが保持する値を更新
  accel.update();
  
  unsigned long now = millis();
  if (now - lastMsg > 5000) {
    //最後のメッセージ送信から5000ミリ秒以上経過していたら
    lastMsg = now;

    // ライブラリが保持しているX軸の加速度を取得しxに代入
    float x = accel.getX();
    //MQTTペイロードを組み立て
    String payload = "{\"device\":\"";
    payload += mqttDeviceId;
    payload += "\",";
    payload += "\"x\":";
    payload += x;
    payload += "}";

    // パブリッシュ
    client.publish(mqttTopic.c_str(), payload.c_str());
    Serial.print(mqttTopic);
    Serial.print(":");
    Serial.println(payload);
  }
}
